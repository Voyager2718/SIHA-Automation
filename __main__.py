from automation import *
from lib import *

import sys

try:
    if len(sys.argv) == 2:
        if sys.argv[1] == 'test':
            try:
                if Mission("echo 'This is a test.'", 'Test').run()[0] is Result.SUCCESS:
                    print(bcolors.OKGREEN +
                          "Basic functional test finished: OK." + bcolors.ENDC)
                    sys.exit(0)
                else:
                    print(
                        bcolors.FAIL + "Basic functional test finished: Failed." + bcolors.ENDC)
                    sys.exit(1)
            except Exception as e:
                print(
                    bcolors.FAIL + "An exception was raised while testing basic functionalities: {0}".format(e) + bcolors.ENDC)
                sys.exit(1)
        else:
            print(bcolors.FAIL + "No action specified." + bcolors.ENDC)
            sys.exit(1)
    elif len(sys.argv) == 3:
        if sys.argv[1] == 'execute':
            (s, msg) = Executor().execute(sys.argv[2])
            succ = (s == Result.SUCCESS)
            if succ:
                print(bcolors.OKGREEN +
                      "All missions finished successfully." + bcolors.ENDC)
                sys.exit(0)
            else:
                print(
                    bcolors.FAIL + "At least one mission failed. Cause: " + msg + bcolors.ENDC)
                sys.exit(1)
    else:
        print(bcolors.FAIL + "No mission specified." + bcolors.ENDC)
        sys.exit(1)
except KeyboardInterrupt:
    print("\nStopped.")
    sys.exit(1)
