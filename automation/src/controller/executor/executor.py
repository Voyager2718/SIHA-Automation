from works import works

from lib import *
from ..mission import *
from ...model import *


class Executor:
    def __init__(self):
        pass

    def execute(self, work):
        try:
            w = works[work]
        except KeyError:
            raise KeyError("No work named \"" +
                           work + "\".")

        missions = []
        try:
            missions = [w["command"]]
            w2 = [w]
        except KeyError:
            try:
                missionNames = w["missions"]
                w2 = []
                for missionName in missionNames:
                    try:
                        mission = works[missionName]
                    except KeyError:
                        raise Exception(bcolors.FAIL + "Mission name \"" +
                                        missionName + "\" not found." + bcolors.ENDC)
                    missions += [mission["command"]]
                    w2 += [mission]
            except KeyError:
                raise KeyError(
                    bcolors.FAIL + "Either \"command\" or \"missions\" is required." + bcolors.ENDC)
            except Exception, e:
                raise KeyError(e)

        try:
            w["command"]
            w["missions"]
            raise Exception(
                bcolors.FAIL + "\"command\" and \"missions\" cannot be presented at the same time." + bcolors.ENDC)
        except KeyError:
            pass

        lastResult = (Result.FAIL, "No valid Mission or Transaction")

        i = 0
        for mission in missions:
            if i is not 0:
                print("")
            print("Mission " + str(i) + "\n-------------")
            try:
                m = Factory.getObject(w2[i]["missionRunner"])
                m.setCommand(mission)
            except KeyError:
                m = Mission(mission)

            needRedirecton = False
            try:
                m.setRedirectToPipe(w2[i]["redirectOutput"])
                needRedirecton = True
            except KeyError:
                pass

            try:
                if type(w2[i]["enters"]) == list:
                    m.setEnterValues(w2[i]["enters"])
                elif type(w2[i]["enters"]) == str:
                    m.setEnterValues([w2[i]["enters"]])
            except KeyError:
                pass

            try:
                if type(w2[i]["expected"]) == list:
                    m.setExpectedExistInOutput(w2[i]["expected"])
                elif type(w2[i]["expected"]) == str:
                    m.setExpectedExistInOutput([w2[i]["expected"]])
                if needRedirecton:
                    raise Exception(
                        bcolors.FAIL + "When \"redirectOutput is <True>, output will not be captured. Hence <expected> will not work.\"" + bcolors.ENDC)
            except KeyError:
                pass

            try:
                if type(w2[i]["unexpected"]) == list:
                    m.setExpectedNotExistInOutput(w2[i]["unexpected"])
                elif type(w2[i]["unexpected"]) == str:
                    m.setExpectedNotExistInOutput([w2[i]["unexpected"]])
                if needRedirecton:
                    raise Exception(
                        bcolors.FAIL + "When \"redirectOutput is <True>, output will not be captured. Hence <unexpected> will not work.\"" + bcolors.ENDC)
            except KeyError:
                pass

            try:
                m.setExpectedReturnValue(w2[i]["returnValue"])
            except KeyError:
                pass

            lastResult = (s, msg) = m.run()
            if s == Result.FAIL:
                return (s, msg)

            i += 1
        return lastResult
