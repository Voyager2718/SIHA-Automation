import subprocess
import re

from ... import Result


class Mission:
    def __init__(self, command="", redirectToPipe=True, enters=[], expected=None, unexpected=None, returnValue=0, succMission=None, failMission=None):
        # Init
        self.__output = ''
        self.__returnValue = -1

        # Assign
        self.__command = command
        self.__expectedOutputs = []
        if unexpected != None:
            self.__expectedOutputs += [expected]
        self.__unexpectedOutputs = []
        if unexpected != None:
            self.__unexpectedOutputs += [unexpected]
        self.__expectedReturnValue = returnValue
        self.__succMissions = []
        if succMission != None:
            self.__succMissions += [succMission]
        self.__failMissions = []
        if failMission != None:
            self.__failMissions += [failMission]
        self.__enterValues = enters
        self.redirect = redirectToPipe

    def setCommand(self, command):
        self.__command = command

    def addEnterValue(self, enter):
        self.__enterValues += [enter]

    def setEnterValues(self, enters):
        self.__enterValues = enters

    def addExpectedExistInOutput(self, pattern):
        self.__expectedOutputs += [pattern]

    def setExpectedExistInOutput(self, expected):
        self.__expectedOutputs = expected

    def addExpectedNotExistInOutput(self, pattern):
        self.__unexpectedOutputs += [pattern]

    def setExpectedNotExistInOutput(self, unexpected):
        self.__unexpectedOutputs = unexpected

    def setExpectedReturnValue(self, value):
        self.__expectedReturnValue = value

    def addNextMissionOnSuccess(self, mission):
        self.__succMissions += [mission]

    def addNextMissionOnFail(self, mission):
        self.__failMissions += [mission]

    def setRedirectToPipe(self, shouldRedirect):
        self.redirect = shouldRedirect

    def getOutput(self):
        return self.__output

    def getReturnValue(self):
        return self.__returnValue

    def run(self):

        print("Executing command <" + self.__command + ">.")

        if self.redirect:
            p = subprocess.Popen(self.__command, stdin=subprocess.PIPE,
                                 stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, bufsize=1)
        else:
            p = subprocess.Popen(
                self.__command, stdin=subprocess.PIPE, shell=True, bufsize=1)

        out = ''    # Collect outputs

        if self.redirect:
            for line in iter(p.stdout.readline, b''):
                out += line
                print "Output> " + line,
            p.stdout.close()
            p.wait()
        else:
            for e in self.__enterValues:
                p.communicate(input=e)
            p.wait()

        self.__output = out
        for e in self.__expectedOutputs:
            if re.search(e, out) == None:
                return (Result.FAIL, "At least one expected value is not found.")

        for ne in self.__unexpectedOutputs:
            if re.search(ne, out) != None:
                return (Result.FAIL, "At least one unexpected value is found.")

        p.wait()
        if p.returncode != self.__expectedReturnValue:
            return (Result.FAIL, "Mission exit with unexpected return code.")

        return (Result.SUCCESS, "")
