from enum import Enum

class Result(Enum):
    SUCCESS = 1
    FAIL    = 2