class Injector:
    @staticmethod
    def createObject(cls, dependencies):
        depObjects = []
        if len(dependencies) > 0:
            for dependency in dependencies:
                depObjects += [Factory.getObject(dependency)]
            return cls(depObjects)
        return cls()


class Factory:
    def __init__(self):
        pass

    @staticmethod
    def getClass(cls):
        parts = cls.split('.')
        module = ".".join(parts[:-1])
        m = __import__(module)
        for comp in parts[1:]:
            m = getattr(m, comp)
        return m

    @staticmethod
    def getObject(cls):
        try:
            c = Factory.getClass(cls)
            dep = c.dependencies
            try:
                return Injector.createObject(c, dep)
            except RuntimeError:
                raise RuntimeError(
                    "A dependency injection loop has been found while trying to inject dependencies.")
        except AttributeError:
            return Factory.getClass(cls)()
