"""
Work can be defined as Mission and Transaction.

Mission:        A command that will be run.
Transaction:    A group of Missions that will be run if the last Mission is finish successfully.

Mission:
<Mission name>: {
    "command": <str>,                               # <Required>    # Command to be run.
    "redirectOutput": <bool>,                       # <Optional>    # If this is True, then the output of a command will be redirected to PIPE. This may cause problem if the command requires to enter passwords. By default this is True.
    "missionRunner": <str>,                         # <Optional>    # If this is present, then Executor will instantiate designated class and let it run mission. The implementation of the class should be put into lib/.
    "enters": <list> or <str>,                      # <Optional>    # Strings that you want to enter during the execution.
    "expected": <list> or <str>,                    # <Optional>    # Expected value of command output. If one of the string in this list failed to be found in the output, it will cause Mission Failed.
    "unexpected": <list> or <str>,                  # <Optional>    # Unwanted value of command output. If one of the string in this list is found in the output, it will cause Mission Failed.
    "returnValue": <integer>                        # <Optional>    # If the command returns a value that is different from the value set here, it will cause Mission Failed.
}

Transaction:
<Transaction name>: {
    "missions": <list of <str>>     # Missions that will be run one by one.
}


Mission sample:
"testMission": {
    "command": "ls /",
    "missionRunner": "SampleMission.SampleMission",
    "redirectOutput": False,
    "enters": "password\n",
    "expected": ["Expected String or regex."],
    "unexpected": ["Unwanted String or regex."],
    "returnValue": 0,
}

Transaction sample:
"testTransaction": {
    "missions": ["testMission", "testMission"]
}
"""


works = {
    "mtest": {
        "command": "echo 'Test'; sleep 1; echo 'Test2'; sleep 1 ; echo 'Test3'",
        "missionRunner": "SampleMission2.SampleMission2",
        "expected": ["Test"],
        "enters": "enter\n",
        "unexpected": ["Unwanted"],
        "returnValue": 0,
    },
    "ttest": {
        "missions": ["mtest", "mtest"]
    },
    "siha-install": {
        "command": "/u01/app/grid/gridSetup.sh -silent -responseFile /scratch/crsusr/siha.rsp",
        "redirectOutput": False,
        "enters": "welcome1\n",
        "returnValue": 0,
    }
}
